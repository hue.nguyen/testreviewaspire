package com.review.testcase;

import org.testng.annotations.Test;

import commons.AbstractPage;
import pageObject.HomePageObject;
import pageObject.InventoryPageObject;
import pageObject.LoginPageObject;
import pageObject.ManufaturingPageObject;
import pageObject.NewManufacturingPageObject;
import pageObject.NewProductObject;
import pageObject.ProductsPageObject;
import pageUIs.LoginPageUI;

import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.jetty.html.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class testcase01 extends AbstractPage{
	String projectFolder = System.getProperty("user.dir");
	WebDriver driver;
	Select select;
	String account, password, urlweb;
	LoginPageObject loginPage;
	HomePageObject homePage;
	InventoryPageObject inventoryPage;
	ProductsPageObject productsPage;
	NewProductObject newProductPage;
	ManufaturingPageObject manufacturingPage;
	NewManufacturingPageObject newManufacturingPage;
	String nameProduct;

	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", projectFolder + "/libraries/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		urlweb = "https://aspireapp.odoo.com/";
		openPageUrl(driver, urlweb);
		account = "user@aspireapp.com";
		password = "@sp1r3app";
		loginPage = new LoginPageObject(driver);
		//name product random
		double randomDouble = Math.random();
		String randomString = Double.toString(randomDouble);
		nameProduct = "hue_test_"+ randomString;
	}

	@Test
	public void TC_01_Login() {
		Assert.assertTrue(loginPage.verifyTitlePageLogin());
		Assert.assertTrue(loginPage.isDisplayedEmail());
		Assert.assertTrue(loginPage.isDisplayedPassWord());
		Assert.assertTrue(loginPage.isDisplayedButtonLogin());
		loginPage.inputEmail(account);
		loginPage.inputPassWord(password);
		loginPage.clickToButtonLogin();
		homePage = new HomePageObject(driver);
	}
	
	@Test
	public void TC_02_home_click_to_change_page_inventory() {
		Assert.assertTrue(homePage.isItemInventoryDisplayed());
		homePage.clickLinkInventory();
		inventoryPage = new InventoryPageObject(driver);
	}
	
	@Test
	public void TC_03_inventory_to_change_page_product() {
		Assert.assertTrue(inventoryPage.verifyTitleInventory());
		inventoryPage.clickProductMenu();
		inventoryPage.clickProductMenuDropdown();
		productsPage = new ProductsPageObject(driver);
	}
	
	@Test
	public void TC_04_create_a_new_product() {
		productsPage.clickButtonCreate();
		newProductPage = new NewProductObject(driver);
	}
	
	@Test
	public void TC_05_create_new_quality_product() {
		newProductPage.inputProductName(nameProduct);
		newProductPage.clickLinkUpdateQuanlity();
		newProductPage.clickCreateButtonNewUpdateQuanlity();
		
		// I can not add 10 quanlities because web had changed
		int numberOfQuanlities = 1; // web only add one product
		for(int i = 0; i < numberOfQuanlities; i ++)		
			newProductPage.addNewproduts();
		newProductPage.clickToSaveProduct();
		click_icon_application();
		homePage = new HomePageObject(driver);
	}
	
	@Test
	public void TC_06_change_to_manufacturing_page() {
		homePage.clickLinkManufacturing();
		manufacturingPage = new ManufaturingPageObject(driver);
		Assert.assertTrue(manufacturingPage.verifyTitleManufaturing());
		manufacturingPage.clickButtonCreateManufacturing();
		newManufacturingPage = new NewManufacturingPageObject(driver);
	}
	
	@Test
	public void TC_07_create_new_quality_manufacturing() {
		newManufacturingPage.chooseProduct(nameProduct);
		sleepInSecond(5);
		newManufacturingPage.clickButtonConfirm();
		sleepInSecond(5);
		newManufacturingPage.clickSaveButton();
		sleepInSecond(5);
		newManufacturingPage.verifyStatusComponents();
	}
	
	@Test
	public void TC_08_verify_successfully() {
		newManufacturingPage.verifyStatusComponents();
		newManufacturingPage.verifyNameProduct(nameProduct);
	}

	public void click_icon_application() {
		homePage.clickIconApplication();
	}
	
//	@AfterClass
//	public void afterClass() {
//		driver.quit();
//	}

}
