#STRUCTURE OF PROJECT
Project includes 4 source folders: 
1.	Testcase
File testcase01.java: write steps of testcases
2.	Action
Package common: includes AbstractPage class: Handling common features and allowing classes to inherit and call functions easily, easy to use
Package pageObject: includes Pages of Website such as: Page Login, Page Home, Page Inventory, Page Products, Page Create a new product, Page Manufacturing, Page create a new Manufacturing
Those pages make actions to handle functions such: click button, verify, enter value, check element…. And is extended from AbstractPage class to call functions, methods from AbstractPage and get Xpath from PagUIs
3.	Resources
Having a file xml to call to run testcase01.java 
This file config the name and location of testcase02.java on this project
4.	Interface
Package pageUIs contains UI of each the above Pages; PageUis includes value of Xpath or Tittle page…


#HOW TO RUN TESTCASE
1.	Setup environment
Make sure that: Setup eclipse and TestNG on Eclipse successfully
2.	Open the project
Get code at the link: https://gitlab.com/hue.nguyen/testreviewaspire
Open Eclipse -> File -> Open Projects from File System -> Enter the link of project -> Finish
3.	Run testcase
Open project on Eclipse -> Open the folder resources -> runtestcase.xml -> Click right mouse -> Run As -> TextNG Suite


#NOTE:
I can not update the quantity of new products more than 10 because the server https://aspireapp.odoo.com/web/login has changed some elements and databases when I’m working. I can only add one new product. But I also write code for this case (adding 10 qualities) if you want to add more than 10 quantities, you can change the value of numberOfQuanlities at the file testcase01.java (Line 96)

