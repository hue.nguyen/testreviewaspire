package pageObject;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import commons.AbstractPage;
import pageUIs.NewProductUI;

public class NewProductObject extends AbstractPage{
	WebDriver driver;
	
	public NewProductObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void inputProductName(String name) {
		// TODO Auto-generated method stub
		sendKeyToElement(driver, NewProductUI.INPUT_PRODUCT_NAME, name);
	}

	public void clickLinkUpdateQuanlity() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, NewProductUI.LINK_UPDATE_QUANLITY);
		clickToElement(driver, NewProductUI.LINK_UPDATE_QUANLITY);
	}

	public void addNewproduts() {
		// TODO Auto-generated method stub
//		int maxlocation = 3;
		int minlocation = 1; // web only one 
//		Random generator = new Random();
//		int location = generator.nextInt((maxlocation - minlocation) + 1) + minlocation;
//		System.out.println(location);
//		locationSelect(location);
		System.out.println(minlocation);
		locationSelect(minlocation);
		double randomDouble = Math.random();
		String quantity = Double.toString(randomDouble);
		System.out.println(quantity);

		sendkeysBoard(driver, Keys.TAB);
		sendkeys(driver, quantity);
		sendkeysBoard(driver, Keys.TAB);
	}
	
	public void locationSelect(int location) {
		for(int i = 0; i < location; i ++) {
			sendkeysBoard(driver, Keys.ARROW_DOWN);
		}
		sendkeysBoard(driver, Keys.ENTER);
	}
	
	public void clickCreateButtonNewUpdateQuanlity() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, NewProductUI.BUTTON_CREATE_NEW_QUANLITY);
		clickToElement(driver, NewProductUI.BUTTON_CREATE_NEW_QUANLITY);
	}

	public void clickToSaveProduct() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, NewProductUI.BUTTON_SAVE_QUANLITY);
		clickToElement(driver, NewProductUI.BUTTON_SAVE_QUANLITY);
	}

}
