package pageObject;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.InventoryPageUI;
import pageUIs.ManufaturingPageUI;

public class ManufaturingPageObject extends AbstractPage {
	WebDriver driver;

	public ManufaturingPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public boolean verifyTitleManufaturing() {
		// TODO Auto-generated method stub
		return isElementDisplayed(driver, ManufaturingPageUI.PRODUCTS_TITLEMANUFACTURING);
	}

	public void clickButtonCreateManufacturing() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, ManufaturingPageUI.BUTTON_CREATE);
		clickToElement(driver, ManufaturingPageUI.BUTTON_CREATE);
		
	}
	
}
