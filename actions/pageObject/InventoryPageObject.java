package pageObject;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.InventoryPageUI;

public class InventoryPageObject extends AbstractPage{
	WebDriver driver;

	public InventoryPageObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void clickProductMenu() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, InventoryPageUI.PRODUCTS_MENU);
		clickToElement(driver, InventoryPageUI.PRODUCTS_MENU);
	}

	public boolean verifyTitleInventory() {
		// TODO Auto-generated method stub
		return isElementDisplayed(driver, InventoryPageUI.PRODUCTS_TITLEINENTORY);
	}

	public void clickProductMenuDropdown() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, InventoryPageUI.PRODUCTS_MENU);
		clickToElement(driver, InventoryPageUI.PRODUCTS_MENUD_DROPDOWNLIST);
		
	}

}
