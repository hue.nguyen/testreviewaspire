package pageObject;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.HomePageUI;

public class HomePageObject extends AbstractPage{
	WebDriver driver;
	public HomePageObject(WebDriver driver) {
		this.driver = driver;
	}
	public void verifyItemInventoryDisplayed() {
		// TODO Auto-generated method stub
		
	}
	public boolean isItemInventoryDisplayed() {
		// TODO Auto-generated method stub
		return isElementDisplayed(driver, HomePageUI.INVENTORY_ICON);
	}
	public void clickLinkInventory() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, HomePageUI.INVENTORY_ICON);
		clickToElement(driver, HomePageUI.INVENTORY_ICON);
	}
	
	public void clickLinkManufacturing() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, HomePageUI.MANUFATURING_ICON);
		clickToElement(driver, HomePageUI.MANUFATURING_ICON);
	}
	public void clickIconApplication() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, HomePageUI.HOME_MENU);
		clickToElement(driver, HomePageUI.HOME_MENU);
		
	}
	

}
