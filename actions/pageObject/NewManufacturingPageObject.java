package pageObject;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.NewManufacturingPageUI;

public class NewManufacturingPageObject extends AbstractPage{
	
	WebDriver driver;
	
	public NewManufacturingPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public void chooseProduct(String nameProduct) {
		// TODO Auto-generated method stub
//		selectItemInDropDown(driver, NewManufacturingPageUI.NAME_PRODUCT, nameProduct);
		System.out.println("aaaaaaa");
		sendKeyToElement(driver, NewManufacturingPageUI.NAME_PRODUCT, nameProduct);
	}

	public void clickButtonConfirm() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, NewManufacturingPageUI.BTN_CONFIRM);
		clickToElement(driver, NewManufacturingPageUI.BTN_CONFIRM);
	}
	
	public boolean verifyStatusComponents() {
		// TODO Auto-generated method stub
		return getTextElement(driver,NewManufacturingPageUI.STATUS_PRODUCT).equals("Available");
	}

	public void clickSaveButton() {
		// TODO Auto-generated method stub
		
		waitToElementClickable(driver, NewManufacturingPageUI.BTN_SAVE);
		clickToElement(driver, NewManufacturingPageUI.BTN_SAVE);
	}

	public boolean verifyNameProduct(String nameProduct) {
		// TODO Auto-generated method stub
		return getTextElement(driver,NewManufacturingPageUI.STATUS_PRODUCT).equals(nameProduct);
		
	}


}
