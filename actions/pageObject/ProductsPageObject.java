package pageObject;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.ProductsPageUI;

public class ProductsPageObject extends AbstractPage{
	
	WebDriver driver;
	
	public ProductsPageObject(WebDriver driver) {
		this.driver = driver;
	}

	public void clickButtonCreate() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver, ProductsPageUI.CREATE_BUTTON);
		clickToElement(driver, ProductsPageUI.CREATE_BUTTON);
	}

}
