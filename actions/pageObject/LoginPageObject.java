package pageObject;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.LoginPageUI;

public class LoginPageObject extends AbstractPage{
	
	WebDriver driver;

	public LoginPageObject(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean verifyTitlePageLogin() {
		// TODO Auto-generated method stub
		return verifyTitlePage(driver, LoginPageUI.TITLE_LOGIN_PAGE);
	}

	public void verifyFieldEmail() {
		// TODO Auto-generated method stub
		Assert.assertTrue(isElementDisplayed(driver, LoginPageUI.EMAIL_INPUT));
	}

	public void verifyFieldPassWord() {
		// TODO Auto-generated method stub
		Assert.assertTrue(isElementDisplayed(driver, LoginPageUI.PASSWORD_INPUT));
	}

	public void verifyButtonLogin() {
		// TODO Auto-generated method stub
		Assert.assertTrue(isElementDisplayed(driver, LoginPageUI.LOGIN_BUTTON));
	}

	public void inputEmail(String account) {
		// TODO Auto-generated method stub
		sendKeyToElement(driver, LoginPageUI.EMAIL_INPUT, account);

	}

	public void inputPassWord(String password) {
		// TODO Auto-generated method stub
		sendKeyToElement(driver, LoginPageUI.PASSWORD_INPUT, password);
		
	}

	public void clickToButtonLogin() {
		// TODO Auto-generated method stub
		waitToElementClickable(driver,LoginPageUI.LOGIN_BUTTON);
		clickToElement(driver, LoginPageUI.LOGIN_BUTTON);
		
	}

	public boolean isDisplayedEmail() {
		// TODO Auto-generated method stub
		return isElementDisplayed(driver, LoginPageUI.EMAIL_INPUT);
	}

	public boolean isDisplayedPassWord() {
		// TODO Auto-generated method stub
		return isElementDisplayed(driver, LoginPageUI.PASSWORD_INPUT);
	}

	public boolean isDisplayedButtonLogin() {
		// TODO Auto-generated method stub
		return isElementDisplayed(driver, LoginPageUI.LOGIN_BUTTON);
	}

}
