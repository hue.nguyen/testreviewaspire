package pageUIs;


public class NewProductUI {
	public static final String INPUT_PRODUCT_NAME = "//div[@class='oe_title']//input";
	public static final String LINK_UPDATE_QUANLITY = "//button//span[contains(text(),'Update Quantity')]";
	public static final String BUTTON_CREATE_NEW_QUANLITY = "//button[@class='btn btn-primary o_list_button_add']";
	public static final String BUTTON_SAVE_QUANLITY = "//button[@class='btn btn-primary o_list_button_save']";
}
