package pageUIs;

public class HomePageUI {
	public static final String TITLE_PAGE = "INVENTORY OVERVIEW - Odoo";
	public static final String INVENTORY_ICON = "//a[@id='result_app_1']";
	public static final String HOME_MENU = "//a[@aria-label='Home menu']";
	public static final String MANUFATURING_ICON = "//a[@id='result_app_2']";
}
