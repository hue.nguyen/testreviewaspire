package pageUIs;

public class InventoryPageUI {
	public static final String PRODUCTS_TITLEINENTORY = "//a[contains(text(),'Inventory')]";
	public static final String PRODUCTS_MENU = "//button[@class='dropdown-toggle  ']//span[contains(text(),'Products')]";
	public static final String CREATE_BUTTON = "//button[@class='btn btn-primary o-kanban-button-new']";
	public static final String PRODUCTS_MENUD_DROPDOWNLIST = "//div[@class='o-dropdown--menu dropdown-menu d-block o-popper-position o-popper-position--bs']//a[contains(text(),'Products')]";
}
