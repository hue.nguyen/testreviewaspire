package pageUIs;

public class LoginPageUI {
	public static final String TITLE_LOGIN_PAGE = "Odoo";
	public static final String EMAIL_INPUT = "//input[@id='login']";
	public static final String PASSWORD_INPUT = "//input[@id='password']";
	public static final String LOGIN_BUTTON = "//button[@class='btn btn-primary btn-block']";

}
