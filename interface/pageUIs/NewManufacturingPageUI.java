package pageUIs;

public class NewManufacturingPageUI {
	public static final String NAME_PRODUCT = "//input[@class='o_input ui-autocomplete-input']";
	public static final String BTN_CONFIRM = "//button[@name='action_confirm']";
	public static final String STATUS_PRODUCT = "//span[@name='components_availability']";
	public static final String BTN_SAVE = "//button[@class='btn btn-primary o_form_button_save']";
	public static final String NAME_PRODUCT_ID = "//a[@name=\"product_id\"]//span";

}
